(require 'org) ;; Org mode support
(require 'htmlize) ;; source code block export to HTML
(require 'ox-publish) ;; publishing functions
(require 'ox-latex) ;; LaTeX publishing functions
(require 'org-ref) ;; bibliography support

;; Disable Babel code evaluation.
(setq org-export-babel-evaluate t)

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((dot . t)))

;; Load style presets.
(load-file "styles/styles.el")

;; Configure HTML website and PDF document publishing.
(setq org-publish-project-alist
      (list
       (list "guidelines"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["guidelines.org"]
             :publishing-function '(org-html-publish-to-html
                                    org-latex-publish-to-pdf-verbose)
             :publishing-directory "./public")       
       (list "figures"
             :base-directory "./figures"
             :base-extension "png\\|jpg\\|gif\\|svg\\|ico"
             :recursive t
             :publishing-directory "./public/figures"
             :publishing-function '(org-publish-attachment))))

(provide 'publish)
