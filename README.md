# Tuto Techno - Guix HPC: Session guidelines

[![pipeline status](https://gitlab.inria.fr/tuto-techno-guix-hpc/guidelines/badges/master/pipeline.svg)](https://gitlab.inria.fr/tuto-techno-guix-hpc/guidelines/-/commits/master)

Discover Guix and Org mode and learn how to build a reproducible research study
thanks to this association of tools in one afternoon.

**[Consult guidelines](https://tuto-techno-guix-hpc.gitlabpages.inria.fr/guidelines/)**
